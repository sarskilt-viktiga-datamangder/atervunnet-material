# Changelog

## [0.9.0] - 2024-11-18

### Added
- Initial version of specification for återvunnet material (recycled materials)
- Added complete CSV schema with 13 attributes, 8 of which are mandatory
- Added example data in three formats:
  - CSV format (Appendix A)
  - JSON format (Appendix B)
  - JSON-LD format (Appendix C)
- Added detailed data model documentation with field descriptions and constraints
- Added example data files showing proper use of the specification
- Added format documentation explaining CSV requirements
- Added DCAT-AP-SE 3.0 integration instructions
- Added comprehensive documentation in Swedish

### Changed
- Updated README.md to reflect the actual project purpose
- Removed default GitLab template content
- Updated spec.json with correct project details and editor information
- Updated spec.html with correct title and version information

### Technical Details
- Added waste category constraints (1-49)
- Added unit constraints (TONNE|KiloGM|M3)
- Added customer category constraints (Person|LocalBusiness|GovernmentOrganization)
- Added postal code validation (10000-99999)
- Defined mandatory fields including source, waste_category, and unit
- Implemented proper namespace definitions for linked data integration