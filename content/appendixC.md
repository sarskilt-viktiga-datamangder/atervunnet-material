# Exempel i JSON-LD

```json
{
  "@context": {
    "schema": "http://schema.org/",
    "dcterms": "http://purl.org/dc/terms/",
    "xsd": "http://www.w3.org/2001/XMLSchema#",
    "rdf": "http://www.w3.org/1999/02/22-rdf-syntax-ns#",
    "qudt": "https://qudt.org/schema/qudt/",
    "unit": "http://qudt.org/vocab/unit/"
  },
  "@type": "schema:LocalBusiness",
  "@id": "556123-4567",
  "dcterms:identifier": "556123-4567",
  "schema:identifier": {
    "@type": "xsd:integer",
    "@value": 1480
  },
  "schema:legalName": {
    "@language": "sv",
    "@value": "Göteborg"
  },
  "rdf:type": {
    "@id": "http://data.europa.eu/6p8/waste/scheme/item15"
  },
  "dcterms:dateSubmitted": {
    "@type": "xsd:dateTime",
    "@value": "2024-11-18T10:30:00"
  },
  "qudt:value": {
    "@type": "xsd:decimal",
    "@value": 125.5
  },
  "qudt:unit": {
    "@id": "http://qudt.org/vocab/unit/TONNE"
  },
  "schema:address": {
    "@type": "schema:PostalAddress",
    "schema:postalCode": "41323",
    "schema:addressLocality": "Majorna-Linné"
  },
  "schema:email": "kontakt@foretag.se",
  "schema:url": {
    "@id": "https://www.foretag.se"
  },
  "schema:telephone": "031-123456",
  "schema:description": [
    {
      "@language": "sv",
      "@value": "Lokalt företag som rapporterar återvunnet material"
    }
  ]
}
```