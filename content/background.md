**Bakgrund**

Syftet med denna specifikation är att beskriva information om återvunnet material på ett enhetligt och standardiserat vis. 

Att öppet publicera data om återvunnet material har flera syften kopplade till hållbarhet och ökad transparens. Genom att göra denna information tillgänglig kan kommuner och företag visa ansvar i sitt miljöarbete, medan allmänheten får en tydlig insyn i hur deras återvinning bidrar till samhällets mål för hållbar utveckling. Detta stöder miljömål som Agenda 2030 och skapar ett ökat medborgarengagemang, där invånare kan följa statistik och förstå effekterna av sina återvinningsinsatser.

Dessutom är det viktigt att forskare och innovatörer får tillgång till denna data för att utveckla bättre metoder för återanvändning och avfallshantering. Öppen data underlättar också för företag att analysera och förbättra sin återvinningsverksamhet.

En specifikation för datamängden säkerställer att informationen är standardiserad och av hög kvalitet. Det gör att data kan jämföras över tid och mellan olika aktörer, vilket möjliggör mer effektiva analyser och beslut inom avfallshantering och miljöplanering. Standardiserade specifikationer ökar också interoperabiliteten mellan olika system och förbättrar kvaliteten på den data som delas och används.

Följande har deltagit:

**[Dataverkstad](https://www.vgregion.se/ov/dataverkstad/)** - Modellering och framtagande.<br>




