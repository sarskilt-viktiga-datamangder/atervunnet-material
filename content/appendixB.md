# Exempel i JSON

Samma exempel som i Appendix A, fast nu som JSON.

```json
{
    "source": "556123-4567",
    "municipality_code": "1480",
    "municipality": "Göteborg",
    "waste_category": "item15",
    "date": "2024-11-18T10:30:00",
    "value": "125.5",
    "unit": "TONNE",
    "postalcode": "41323",
    "administrative_area": "Majorna-Linné",
    "customer_category": "LocalBusiness",
    "email": "kontakt@foretag.se",
    "URL": "https://www.foretag.se",
    "telephone": "031-123456"
}
```
