## Exempel i CSV, kommaseparerad

<div class="example csvtext">
source,municipality_code,municipality,waste_category,date,value,unit,postalcode,administrative_area,customer_category,email,url,telephone
<br>
556123-4567,1480,Göteborg,15,2024-11-18T10:30:00,125.5,TONNE,41323,Majorna-Linné,LocalBusiness,kontakt@foretag.se,https://www.foretag.se, 031-123456
</div>

### Förtydligande

- Organisationsnummer (source): 556123-4567
- Kommunen är Göteborg
- Avfallskategori 15 (som mappas till en specifik EU-avfallstyp)
- Rapporterar 125.5 ton material
- Beläget i Majorna-Linné stadsdel
- Klassificerat som ett LocalBusiness (lokalt företag)
- Innehåller fullständig kontaktinformation


Datan följer alla begränsningar i schemat:

- Postnummer är inom giltigt intervall (10000-99999)
- Kundkategorier använder giltiga värden (Person, LocalBusiness, GovernmentOrganization)
- Enheter använder giltiga värden (TONNE, KiloGM, M3)
- Datum är i dateTime-format


