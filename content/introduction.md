# Introduktion 

Denna specifikation anger vilka fält som är obligatoriska och rekommenderade. T.ex. gäller att man måste ange source, waste_category och unit. 13 attribut kan anges varav 8 är obligatoriska. 

I appendix A finns ett exempel på hur en entitet uttrycks i CSV. I appendix B uttrycks samma exempel i JSON och i appendix C enligt JSON-LD.

Denna specifikation definierar en enkel tabulär informationsmodell för återvunnet material. Specifikationen innefattar också en beskrivning av hur informationen uttrycks i formaten CSV och JSON. Som grund förutsätts CSV kunna levereras då det är ett praktiskt format och skapar förutsägbarhet för mottagare av informationen. 
