# Datamodell

Datamodellen är tabulär, det innebär att varje rad motsvarar exakt en entitet och varje kolumn motsvarar en egenskap för den beskrivna entiteten. 13 attribut är definierade, där de första 8 är obligatoriska. Speciellt viktigt är att man anger de obligatoriska fält i datamodellen som anges. 


<div class="note" title="1">
Vi väljer att använda beskrivande men korta kolumnnamn som uttrycks med gemener, utan mellanslag (understreck för att separera ord) och på engelska.

Genom engelska attributnamn blir modellen enklare att hantera i programvaror och tjänster utvecklade utanför Sverige.

</div>

<div class="ms_datatable">

| Namn  | Kardinalitet      | Datatyp                         | Beskrivning|
| -- | :---------------:| :-------------------------------: | ---------------------- |
|[**source**](#source)|1|[heltal](#heltal)|**Obligatoriskt** - Organisationsnummer för organisationen som är ansvarig för inrapportering av avfallsinformation.|
|[**municipality_code**](#municipality_code)|1|[heltal](#heltal)|**Obligatoriskt** - Kommunens kod, som den inrapporterade avfallsinformationen avser.|
|[**municipality**](#municipality)|1|[text](#text)|**Obligatoriskt** - Kommunens namn, som den inrapporterade avfallsinformationen avser. |
|[**waste_category**](#waste_category)|1|[concept](#concept)|**Obligatoriskt** - Avfallskategori, avser vilken kategori av avfall det är. Följer EU-standard och vokabulär. Exempelvis "Plastic wastes".|
|[**date**](#date)|1|[datetime](#datetime)|**Obligatoriskt** - Datum för den inrapporterade avfallsinformationen. Se förtydligande av dataformat nedan. Exempelvis "2024-09-11"|
|[**value**](#value)|1|[decimal](#value)|**Obligatoriskt** - Den inrapporterade avfallsinformationens värde. Exempelvis "12345".|
|[**unit**](#unit)|1|[concept](#unit)|**Obligatoriskt** - Den inrapporterade avfallsinformationens typ, Vikt eller Volym. Exempelvis "Vikt"|
|[postalcode](#postalcode)|0..1|[heltal](#heltal)|Postnummer för den inrapporterade avfallsinformationen.|
|[administrative_area](#administrative_area)|0..1|[text](#text)|Distrikt eller stadsdel för den inrapporterade avfallsinformationen.|
|[customer_category](#customer_category)|0..n|[concept](#concept)|Kundkategori för den inrapporterade avfallsinformationen.|
|[email](#email)|0..1|[text](#text)|E-postadress till den inrapporterande organisationen.|
|[URL](#url)|0..1|[URL](#url)|Ingångssida för mer information om organisationen ansvarig för den inrapporterade avfallsinformationen.|
|[telephone](#telephone)|0..1|[text](#text)|Telefonnummer till organisationen ansvarig för den inrapporterade avfallsinformationen.|

</div>

## Förtydligande av datatyper

En del av datatyperna nedan förtydligas med hjälp av det som kallas reguljära uttryck. Dessa är uttryckta så att de matchar exakt, dvs inga inledande eller eftersläpande tecken tillåts.

### **heltal**
Reguljärt uttryck: **`/^\-?\\d+$/`**

Heltal anges alltid som en radda siffror utan mellanrum eventuellt med ett inledande minus. Se [xsd:integer](https://www.w3.org/TR/xmlschema-2/#integer) för en längre definition. Utelämnat värde tolkas aldrig som noll (0) utan tolkas som “avsaknad av värde”.
 
 ### **dateTime** 

Reguljärt uttryck: **`\d{4}-[01]\d-[0-3]\dT[0-2]\d:[0-5]\d:[0-5]\d(?:\.\d+)?Z?`**

dateTime värden kan ses som objekt med heltalsvärdena år, månad, dag, timme och minut, en decimalvärderad sekundegenskap och en tidszonegenskap.

[XML schema](https://www.w3.org/TR/xmlschema-2/#dateTime) 
</br>

[IS0 8601](https://en.wikipedia.org/wiki/ISO_8601)

Värden som kan anges för att beskriva ett datum:

YYYY-MM-DD
</br>
YYYY-MM
</br>
YYYYMMDD 
<!-- </br> -->
</div>

Observera att ni inte kan ange: **YYYYMM**    

För att beskriva tider uttrycker ni det enligt nedan:

**T** representerar tid, **hh** avser en nollställd timme mellan 00 och 24, **mm** avser en nollställd minut mellan 00 och 59 och **ss** avser en nollställd sekund mellan 00 och 60.

Thh:mm:ss.sss eller	Thhmmss.sss 
</br>
Thh:mm:ss eller Thhmmss
</br>
Thh:mm eller Thhmm
</br>

</div>

Exempel - XX som äger rum kl 17:00 den 22 februari 2023 

</br>
2024-02-22T17:00

</br>
</br>

20240222T1700

</div>

### **decimal**
Reguljärt uttryck: **`/^\-?\\d+\\.\\d+$/`**

Decimaltal anges i enlighet med [xsd:decimal](https://www.w3.org/TR/xmlschema-2/#decimal). Notera att i Sverige används ofta decimalkomma inte punkt. För att vara enhetlig mellan olika dataformat ska decimalpunkt användas då den tabulära modellen använder komma som separator.

Den kanoniska representationen i xsd:decimal är påbjuden, d.v.s. inga inledande nollor eller +, samt att man alltid ska ha en siffra innan och efter decimalpunkt. Noll skrivs som 0.0 och ett utelämnat värde skall aldrig tolkas som noll (0) utan “avsaknad av värde”.

### **url**

En länk till en webbsida där entiteten presenteras hos den lokala myndigheten eller organisationen.

Observera att man inte får utelämna schemat, d.v.s. "www.example.com" är inte en tillåten webbadress, däremot är "https://www.example.com" ok. Relativa webbadresser accepteras inte heller. (Ett fullständigt reguljärt uttryck utelämnas då den är både för omfattande och opedagogisk.)

Om du behöver ange flera URL:er måste du då sätta dubbelt citattecken och separera de olika URL:erna med kommatecken. Läs gärna med i RFC 4180 för CSV.

Kontrollera även innan du publicerar filen att det går att läsa in din fil utan problem. Det finns flera webbverktyg för att testa så kallad [parsing](https://sv.wikipedia.org/wiki/Parser) t.ex. [https://CSVLint.io](https://csvlint.io) som du kan använda för att testa din fil.




## Förtydligande av attribut


### source

Organisationsnumret till organisationen som ansvarar för information om det inrapporterade avfallet. Anger organisationsnummret utan mellanslag eller bindesstreck för organisationen. Detta kan vara en kommun, avfallsbolag eller annan aktör som ansvarar för insamling och återvinning av materialet. Exempelvis "Telge Återvinning".

### municipality_code

 En unik kod som identifierar vilken kommun återvinningsstationen tillhör. Varje kommun i ett land har en specifik kod som används för administrativa och statistiska ändamål. Detta attribut gör det möjligt att koppla återvinningsstationen till en specifik lokal förvaltning, vilket kan vara viktigt för regional planering och rapportering. Använd befintliga kommunkoder, definierade av SCB. Listor med aktuella och officiella kommunkoder hittar ni på SCB's hemsida. Exempelvis "0181".

### municipality

Namnet på den kommun där återvinningsstationen är belägen. Kommunen är den lokala förvaltningsenheten som ansvarar för tjänster som avfallshantering och återvinning. Namnet gör det tydligt för användare vilken lokal förvaltning som ansvarar för återvinningsstationen och dess tjänster.
Kommunnamnet skrivs med fördel i kortform, exempelvis "Södertälje". Dvs inte med tillägget "kommun" som i "Södertälje kommun". Detta i enlighet med Eurostat och ett antal SOS-myndigheter (producenter av Sveriges Officiella Statistik).

### waste_category

Begreppslista, som följer EU-standard, för avfallskategorier finns på [dataportal.se](https://www.dataportal.se/concepts?p=1&q=&s=2&t=20&f=&rt=term&c=false). 
Mer info om EU-standard och vokabulär gällande avfallskategorier hittar du även på [EU's hemsida](https://op.europa.eu/en/web/eu-vocabularies/dataset/-/resource?uri=http://publications.europa.eu/resource/dataset/waste-category).


### waste_type

Avfallstyp, beskriver vilken övergripande kategori av avfall entiteten kommer från. Exempelvis "Hushållsavfall".

### waste_fraction

Avfallsfraktion, avser en mer granulär och specifik beskrivning av avfallstypen och vilket material den består av. Exempelvis "Pappersförpackningar". 
Läs mer om aktuella fraktioner på [Sveriges avfallsportal](https://sopor.nu)


### date

Datumet då materialet samlades in eller rapporterades som återvunnet. Detta datum är viktigt för att förstå tidstrender i avfallshanteringen.


### value

Mängden material som återvunnits, uttryckt i den enhet som anges i attributet Unit. Detta är ett kvantitativt mått och kan representera volym, vikt eller antal beroende på vilken typ av avfall som mäts. Exempelvis "12000".


### unit

Den inrapporterade avfallsinformationens typ. Antingen Vikt eller Volym. Enheten som används för att uttrycka mängden återvunnet material. Det kan vara kilo (kg), ton, kubikmeter (m³) eller en annan standardiserad enhet beroende på vilken typ av avfall som hanteras.

### postalcode

Postnumret för området där återvinningsstationen är belägen. Detta hjälper till att geografiskt avgränsa stationens plats. Ange inte mellanslag. Exempelvis "15132"

### administrative_area

 Det administrativa området där återvinningen har ägt rum, vilket kan vara en stadsdel, förvaltningsområde eller region. Det ger en ytterligare dimension av geografisk uppdelning för att förstå var insamlingen skett. Exempelvis "Västergård"

### customer_category

Kategorin av kund eller verksamhet som har bidragit till det återvunna materialet. Det kan till exempel skiljas på hushållsavfall och industriavfall, eller mellan privatpersoner och företag. Exempelvis "Företag", "Privatperson", "Kommun".

### email

E-postadress, till den inrapporterande organisationen, för vidare kontakt. Anges med gemener och med @ som avdelare. Ange ej det inledande URI schemat mailto: eller HTML-koder i uttrycket. Exempel: example@domain.se

### URL

Ingångssida för mer information om organisationen ansvarig för den inrapporterade avfallsinformationen. Ange inledande schemat https:// eller http://

### telephone

Telefonnummer till organisationen ansvarig för den inrapporterade avfallsinformationen.

 




